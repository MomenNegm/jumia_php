<?php
	// Include CORS headers
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: X-Requested-With');
	header('Content-Type: application/json');

	include_once 'loginReturn.php';
	$user        = new returnLogin();

	$api     = $_SERVER['REQUEST_METHOD'];

	if ($api == 'POST') 
	{
	    $email = $user->test_input($_POST['email']);
	    if ($data = $user->post_login($email)) {
				 echo json_encode($data);
		}
	}

?>
