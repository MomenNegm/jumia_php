
## About tasks

-- added config file that includes db connection configurations
-- added databse.php file containing sql queries needed to take actions on module 
-- added .htaccess to remove .php extenstion from url and access routes directly
-- added validation file to check for parameters needed before submit
-- each user can access jobs according to his type (Role)
-- manager user can access all jobs
-- regular user can access and edit only jobs he created earlier


### how to get started

- put project inside htdocs
- can import databse using attached sql file 
- database contains two table
- table users contains (name, email, phone, type)
- table jobs contains (title, description, status, user_id)
- regualr user (regular@examle.com)
- manager user (manager@examle.com)
- login right now is handled by email only

### Api Postman Requests

- http://localhost/jumia_php/login >> method post 
- login first to grt api_token and sent it as a header value with authorization 
- Authorization key, value Bearer api_token

- http://localhost/jumia_php/jobs  >>> method get 
- if did not get any id via url it will redirect to all jobs 
- if it got id via url it will redirect to this specific job i.e >>> http://localhost/JTask/jobs/1

- both of two requests should contain header Authorization 
- i replaced token by user_id to fetch logged in user jobs according to his type
- for example Authorization as key , Bearer 1 as value 
- it will check for user that matches this user_id 
- and then fetch data

- http://localhost/jumia_php/jobs  >>> method post
- this request is responsible for creating post 
- parameters needed are title, description, and header Authorization Bearer token

- http://localhost/jumia_php/jobs/{id} > method put > parameters should be sent with body >> x-www-form-urlencoded on postman
- is responsible for updating job
- http://localhost/jumia_php/jobs/{id} > method delete
- is responsible for deleting job



