<?php 

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
include_once 'database.php';

final class DependencyFailureTest extends TestCase
{
    public function testFetch(): void
    {
    	$job    = new Database();
    	// admin access all jobs
        $result = $job->fetch(0, 1);
        // returned data
        $this->assertArrayHasKey('data', $result);
    }
}