<?php 

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
include_once 'database.php';

final class CreateJobTest extends TestCase
{
    public function testCreate(): void
    {
    	$job    = new Database();
    	// form data that are passed to method
    	// faker are used for this purpose
        $result = $job->insert('title', 'description', 1);
        // $result is 1 which is boolean true
        $this->assertTrue($result);

        // DB transactions or testing database should be used in order not to affect real database
    }
}