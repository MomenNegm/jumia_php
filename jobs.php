<?php
	// Include CORS headers
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
	header('Access-Control-Allow-Headers: X-Requested-With');
	header('Content-Type: application/json');

	// Include action.php file
	include_once 'database.php';
	include_once 'validation.php';
	include_once 'loginReturn.php';

	// Create object of jobs class
	$job        = new Database();
	$validation = new Validation();
	$Auth       = new returnLogin();

	// create a api variable to get HTTP method dynamically
	$api     = $_SERVER['REQUEST_METHOD'];
	$headers = getallheaders();
	$token   = substr($headers['Authorization'], 7);

	// get id from url
	$id = intval($_GET['id'] ?? '');

	// Get all or a single job from database
	if ($api == 'GET') 
	{
		$loggedIn   = $validation->validateLoggedIn($token);
		if($loggedIn != true){
		  	echo $validation->messageName();
		}else{
			  $user_id     = $Auth->authed_user($token);
			  if ($id != 0) {
			    $data = $job->fetch($id, $user_id);
			  } else {
			    $data = $job->fetch(0, $user_id);
			  }
		}
	  echo json_encode($data);
	}

	// Add a new job into database
	if ($api == 'POST') 
	{
	  $resultName = $validation->validateName($_POST['title']);
	  $loggedIn   = $validation->validateLoggedIn($token);
	  if ($resultName != true) {
	  	echo $validation->messageName();
	  }elseif($loggedIn != true){
	  	echo $validation->messageName();
	  }
	  else{
	  	      $user_id     = $Auth->authed_user($token);
			  $title       = $job->test_input($_POST['title']);
			  $description = $job->test_input($_POST['description']);
			//  $user_id     = $job->test_input($_POST['user_id']);

				if ($job->insert($title, $description, $user_id)) {
				    echo $job->message('Job added successfully!',false);
			    } else {
				    echo $job->message('Failed to add a job!',true);
				}
		}
	}

	// Update job in database
	if ($api == 'PUT') {
	  parse_str(file_get_contents('php://input'), $post_input);

	  $title        = $job->test_input($post_input['title']);
	  $description = $job->test_input($post_input['description']);
	  $user_id     = $Auth->authed_user($token);
	 // $user_id     = $job->test_input($post_input['user_id']);

	  if ($id != null) {
	    if ($job->update($title, $description, $user_id, $id)) {
	      echo $job->message('job updated successfully!',false);
	    } else {
	      echo $job->message('Failed to update ajob!',true);
	    }
	  } else {
	    echo $job->message('Job not found!',true);
	  }
	}

	// Delete job from database
	if ($api == 'DELETE') {
	  if ($id != null) {
	    if ($job->delete($id)) {
	      echo $job->message('job deleted successfully!', false);
	    } else {
	      echo $job->message('Failed to delete ajob!', true);
	    }
	  } else {
	    echo $job->message('job not found!', true);
	  }
	}

?>