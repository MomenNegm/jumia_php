<?php
	
class Validation 
	{
		protected $content = '';
		protected $status  = '';
			  // Insert an user in the database
			  public function validateName($name) 
			  {
			  	if(strlen($name) <= 0)
			  	{
			  		$this->content = 'you should add title';
			  		$this->status  = 400;
			  		return false;
			  	}
			  	elseif(strlen($name) > 100)
			  	{
			  		$this->content = 'title should not esceed 100 characters';
			  		$this->status  = 400;
			  		return false;
			  	}else{
			  		return true;
			  	}
			  }

			  public function validateDescription($description) 
			  {
			  	if(strlen($description) <= 0){
			  		$this->content = 'you should add description';
			  		$this->status  = 400;
			  		return false;
			  	}else{
			  		return true;
			  	}
			  }

			  public function validateLoggedIn($user_id) 
			  {
			  	if(strlen($user_id) <= 0){
			  		$this->content = 'you should login to complete process';
			  		$this->status  = 400;
			  		return false;
			  	}else{
			  		return true;
			  	}
			  }

			  public function messageName()
			  {
			  	return json_encode(['message' => $this->content, 'error' => $this->status]);
			  }
	}

?>