<?php
	// Include config.php file
	include_once 'config.php';

	// Create a class jobs
	class Database extends Config 
	{
			  // Fetch all or a single job from database
			  public function fetch($id, $user_id) {
			  	    $loggedUser = 'SELECT * FROM users where id ='. $user_id;
			  	    $stmtuser = $this->conn->prepare($loggedUser);
			        $stmtuser->execute();
			        $rowsuser = $stmtuser->fetchAll();
			        $type     = $rowsuser[0]['type'];
			    if ($type == 'manager') {
			  		$sql = 'SELECT * FROM jobs';
			  	}else{
			  		$sql = 'SELECT * FROM jobs where user_id ='. $user_id . '';
			  	}
			   
			    if ($id != 0) 
			    {
			    	if ($type == 'manager') {
			  		    $sql .= ' WHERE id = :id';
			  	    }else{
			  		    $sql .= ' AND id = :id';
			  	    }
			      $stmt = $this->conn->prepare($sql);
			      $stmt->execute(['id' => $id]);
			      $rows = $stmt->fetchAll();
			    }else{
			    	$stmt = $this->conn->prepare($sql);
			        $stmt->execute();
			        $rows = $stmt->fetchAll();
			    }
			    $data = array('data' => $rows);
			    return $data;
			  }

			  // Insert an job in the database
			  public function insert($title, $description, $user_id) {
			    $sql = 'INSERT INTO jobs (title, description, user_id) VALUES (:title, :description, :user_id)';
			    $stmt = $this->conn->prepare($sql);
			    $stmt->execute(['title' => $title, 'description' => $description, 'user_id' => $user_id]);
			    return true;
			  }

			  // Update an job in the database
			  public function update($title, $description, $user_id, $id) {
			    $sql = 'UPDATE jobs SET title = :title, description = :description, user_id = :user_id WHERE id = :id';
			    $stmt = $this->conn->prepare($sql);
			    $stmt->execute(['title' => $title, 'description' => $description, 'user_id' => $user_id, 'id' => $id]);
			    return true;
			  }

			  // Delete an job from database
			  public function delete($id) {
			    $sql = 'DELETE FROM jobs WHERE id = :id';
			    $stmt = $this->conn->prepare($sql);
			    $stmt->execute(['id' => $id]);
			    return true;
			  }
	}

?>